from rest_framework import serializers
from courses.models import Course,Video,Assignment

class CourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = '__all__'



class VideoSerializers(serializers.ModelSerializer):

    class Meta:
        model = Video
        fields = '__all__'



class AssignmentSerializers(serializers.ModelSerializer):

    class Meta:
        model = Assignment
        fields = '__all__'
