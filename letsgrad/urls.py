"""letsgrad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from courses import views
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),
    # COURSE
    url('course_list/$', views.CourseListView.as_view(), name='list'),
    url('create/$', views.CourseCreateView.as_view(), name='create'),
    url('details/(?P<id>[0-9]+)/$', views.CourseDetailView.as_view(), name='details'),
    url('update/(?P<id>[0-9]+)/$', views.CourseUpdateView.as_view(), name='update'),
    url('delete/(?P<id>[0-9]+)/$', views.CourseDeleteView.as_view(), name='delete'),
    # VIDEO
    url('video_list/$', views.VideoListView.as_view(), name='video_list'),
    url('video_creates/$', views.VideoCreateView.as_view(), name='video_create'),
    url('video_detailss/(?P<id>[0-9]+)/$', views.VideoDetailView.as_view(), name='video_details'),
    url('video_deletes/(?P<id>[0-9]+)/$', views.VideoDeleteView.as_view(), name='video-delete'),
    url('video_updates/(?P<id>[0-9]+)/$', views.VideoUpdate.as_view(), name='video-update'),
    # ASSIGNMENT
    url('assignment_llist/$', views.AssignmentListView.as_view(), name='assighment_llist'),
    url('assignment_createS/$', views.AssignmentCreateView.as_view(), name='assignment_create'),
    url('assignment_detailS/(?P<id>[0-9]+)/$', views.AssignmentDetailView.as_view(), name='assignment_details'),
    url('assignment_updateS/(?P<id>[0-9]+)/$', views.AssignmentUpdateView.as_view(), name='assignment_update'),
    url('assignment_deleteS/(?P<id>[0-9]+)/$', views.AssignmentDeleteView.as_view(), name='assignment_delete'),

]
